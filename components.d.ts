import testParse from "../mail-builder/src/testParse"

export interface Component {
  text: string
  html: string
  translations: string[]
  parse?(args: {
    html: string
    options: MailBuilder.BuildOptions
    modulName: string
    test: boolean
  }): MailBuilder.Template
}
export interface MailTemplateComponent extends Component {
  prototype(): any
  add({ component: Component }): void
}
export interface TextLink {
  text: string
  url: string
  title: string
  target?: string
}
export interface ImageLink {
  src: string
  url: string
  title: string
  target?: string
}

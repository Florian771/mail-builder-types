import { LogoSettings } from "../mail-builder/src/templates/components/logoSection"

export {
  AlternativeHtmlInfoSettings,
  AlternativeHtmlInfo
} from "../mail-builder/src/templates/components/AlternativeHtmlInfo"
export {
  ButtonSettings,
  Button
} from "../mail-builder/src/templates/components/button"
export {
  HeaderSettings,
  Header
} from "../mail-builder/src/templates/components/header"
export {
  HeaderDesignerSettings,
  HeaderDesigner
} from "../mail-builder/src/templates/components/headerDesigner"
export {
  SectionBottomSettings,
  SectionBottom
} from "../mail-builder/src/templates/components/sectionBottom"
export {
  FooterSettings,
  Footer
} from "../mail-builder/src/templates/components/footer"
export {
  AccordeonSettings,
  Accordeon
} from "../mail-builder/src/templates/components/accordeon"
export {
  DividerSettings,
  Divider
} from "../mail-builder/src/templates/components/divider"
export {
  CarouselSettings,
  Carousel
} from "../mail-builder/src/templates/components/carousel"
export {
  CarouselDesignerSettings,
  CarouselDesigner
} from "../mail-builder/src/templates/components/carouselDesigner"
export {
  HeadlineSettings,
  Headline
} from "../mail-builder/src/templates/components/headline"
export {
  HeadlineWithSublineSettings,
  HeadlineWithSubline
} from "../mail-builder/src/templates/components/headlineWithSubline"
export {
  HeadlineWithSublineWithLinkSettings,
  HeadlineWithSublineWithLink
} from "../mail-builder/src/templates/components/HeadlineWithSublineWithLink"
export {
  HeroSettings,
  Hero
} from "../mail-builder/src/templates/components/hero"
export {
  ImageSettings,
  Image
} from "../mail-builder/src/templates/components/image"
export {
  ImageTextSettings,
  ImageText
} from "../mail-builder/src/templates/components/imageText"
export {
  LogoSettings,
  Logo
} from "../mail-builder/src/templates/components/Logo"
export {
  SocialSettings,
  Social
} from "../mail-builder/src/templates/components/Social"
export {
  SpacerSettings,
  Spacer
} from "../mail-builder/src/templates/components/Spacer"
export {
  SublineSettings,
  Subline
} from "../mail-builder/src/templates/components/Subline"
export {
  TextSettings,
  Text
} from "../mail-builder/src/templates/components/text"
export {
  TextWithImageSettings,
  TextWithImage
} from "../mail-builder/src/templates/components/textWithImage"
export {
  TableBillSettings,
  TableBill
} from "../mail-builder/src/templates/components/tableBill"
export {
  ZusatzTextBilder
} from "../mail-builder/src/templates/components/textWithImage"
export * from "./components"

export type BuildType = "mail" | "file" | "html"

export interface Mail {
  subject: string
  from: string
  to: string
  text: string
  html: string
  options?: BuildOptions
}

export type MailLinkType = "link" | "button"

export interface MailLink {
  type: MailLinkType
  url?: string
  text?: string
  textLink?: string
  options?: BuildOptions
}

export type ContentType =
  | "userCreated"
  | "accountActivation"
  | "hotelRoomOffer"
  | "dynamic"

export type TemplateTypeProduct = {
  name: "product"
  accordeonImages: 4
  menuLinks: 3
  alternativeHtmlInfo: AlternativeHtmlInfoSettings
  header: HeaderSettings
  hero: HeroSettings
  text: TextSettings
  button: ButtonSettings
  carousel: CarouselSettings
  accordeon: AccordeonSettings
  image?: ImageSettings
  imageText?: ImageTextSettings
  text2?: TextSettings
  sectionBottom: SectionBottomSettings
  footer: FooterSettings
  tableBill?: TableBill
  spacer?: SpacerSettings
  textWithImage?: TextWithImageSettings
  logo?: LogoSettings
}
export type TemplateTypeDynamic = {
  name: "dynamic"
  accordeonImages: 4
  menuLinks: 3
  alternativeHtmlInfo: AlternativeHtmlInfoSettings
  header: HeaderSettings
  hero: HeroSettings
  text: TextSettings
  button: ButtonSettings
  carousel: CarouselSettings
  accordeon: AccordeonSettings
  image?: ImageSettings
  imageText?: ImageTextSettings
  text2?: TextSettings
  sectionBottom: SectionBottomSettings
  footer: FooterSettings
  tableBill?: TableBillSettings
  spacer?: SpacerSettings
  textWithImage?: TextWithImageSettings
  logo?: LogoSettings
}

export type TemplateTypeZahlungsErinnerung = {
  name: "zahlungsErinnerung"
  alternativeHtmlInfo: AlternativeHtmlInfoSettings
  header: HeaderSettings
  text: TextSettings
  button?: ButtonSettings
  sectionBottom: SectionBottomSettings
  footer: FooterSettings
  accordeon?: AccordeonSettings
  hero?: HeroSettings
  carousel?: CarouselSettings
  image?: ImageSettings
  imageText?: ImageTextSettings
  text2: TextSettings
  tableBill?: TableBill
  spacer?: SpacerSettings
  textWithImage?: TextWithImageSettings
  logo?: LogoSettings
}

export type TemplateTypeMinimal = {
  name: "minimal"
  alternativeHtmlInfo: AlternativeHtmlInfoSettings
  header: HeaderSettings
  text: TextSettings
  button: ButtonSettings
  sectionBottom: SectionBottomSettings
  footer: FooterSettings
  accordeon?: AccordeonSettings
  hero?: HeroSettings
  carousel?: CarouselSettings
  image?: ImageSettings
  imageText?: ImageTextSettings
  text2?: TextSettings
  tableBill?: TableBill
  spacer?: SpacerSettings
  textWithImage?: TextWithImageSettings
  logo?: LogoSettings
}

export type TemplateType =
  | TemplateTypeMinimal
  | TemplateTypeProduct
  | TemplateTypeDynamic
  | TemplateTypeZahlungsErinnerung

export interface MailContentsInput {
  options?: BuildOptions
}

export interface MailContentsOutput {
  subject: string
  links?: MailLink[]
  text: string
  html: string
  translations: string[]
  options?: BuildOptions
}

export interface TemaplateBuilderResult {
  text: string
  html: string
  translations: string[]
  errors: string[]
}

export interface ComposeMailResult {
  mail: Mail
  translations: string[]
}
export interface Template {
  text: string
  html: string
  errors: string[]
  options?: BuildOptions
}

export interface DesignSettings {
  TemplateName?: string
  primaryColor?: string
  secundaryColor?: string
  sec_textColor?: string
  headlineColor?: string
  textColor?: string
  textColorHighlight?: string
  backgroundColor?: string
  backgroundColor2?: string
  headlineFontName?: string
  bodyFontName?: string
  bottomSlanted?: boolean
  bckg_Color4?: string
}

export interface GeneralSettings {}

export interface Section {
  type: string
  settings?: any
  inverseColors?: "true" | "false"
}

export interface Translation {
  [key: string]: string
}

export interface BuildOptions {
  name: string
  from: string
  to: string
  subject?: string
  data: any
  designSettings: DesignSettings
  templateType: TemplateType
  contentType: ContentType
  buildType: BuildType
  general?: GeneralSettings
  testParse: boolean
  translations?: Translation[]
  sections?: Section[]
  socialMedia?: SocialMedia[]
  zusatzTextBilder?: ZusatzTextBilder[]
  getSingleComponent?: boolean
}

export interface SendResponse {
  statusCode: number
  statusMessage: string
  body: string
}

export interface Result {
  mail: Mail
  translations: string[]
  url: string
  type: BuildType
  sendResponse?: SendResponse
}

export as namespace MailBuilder
